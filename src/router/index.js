import Vue from 'vue'
import Router from 'vue-router'
import Poker from '@/components/Poker'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Poker',
      component: Poker
    }
  ]
})
